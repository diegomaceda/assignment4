import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = "CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating INT UNSIGNED, PRIMARY KEY (id))"

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_table global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_to_db', methods=['POST'])
def add_to_db():
    print("Received request.")
    print(request.form['message'])
    msg = request.form['message']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cnx.commit()
    return hello()

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received POST request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT * FROM movies WHERE title = '" + title + "'")
    except Exception as exp:
        return render_template('index.html', message = exp)

    data = cur.fetchall()
    
    if data:
        return render_template('index.html', message = "Movie with title " + title + " already exists")

    try:
        cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values ('" + year  + 
    "', '" + title  + "', '"+ director  + "', '"+ actor  + "', '" + release_date  + "', '" + rating  + "')")
    except Exception as exp: 
        return render_template('index.html', message = "Movie " + title + " could not be inserted - " + exp)

    cnx.commit()
    return render_template('index.html', message = "Movie " + title + " successfully inserted")

@app.route('/update_movie', methods=['POST'])
def update_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT * FROM movies WHERE title = '" + title + "'")
    except Exception as exp:
        return render_template('index.html', message = exp)

    data = cur.fetchall()
    
    if not data:
        return render_template('index.html', message = "Movie with title " + title + " does not exist")

    try:
        cur.execute("UPDATE movies SET year = '" + year + "', title = '" + title + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "' WHERE title = '" + title + "'")
    except Exception as exp:
        return render_template('index.html', message = "Movie " + title + " could not be updated - " + exp)

    cnx.commit()
    return render_template('index.html', message = "Movie " + title + " successfully updated")

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    delete_title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT * FROM movies WHERE title = '" + delete_title + "'")
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    data = cur.fetchall()
    
    if not data:
        return render_template('index.html', message = "Movie with title " + delete_title + " does not exist")
    
    try:
        cur.execute("DELETE FROM movies WHERE title = '" + delete_title + "'")
    except Exception as exp:
        return render_template('index.html', message = "Movie " + delete_title + " could not be deleted - " + exp)

    cnx.commit()
    return render_template('index.html', message =  "Movie " + delete_title + " successfully deleted")


@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received GET request.")
    actor = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()  
    try:
        cur.execute("SELECT title, year, actor FROM movies WHERE actor = '" + actor + "'")
    except Exception as exp:
        return render_template('index.html', message = exp)

    data = cur.fetchall()
    result = ""
    
    if not data:
        return render_template('index.html', message = "No movies found for actor " + actor)
    
    for row in data:
        result += row[0] + " "
        result += str(row[1]) + " "
        result += row[2] + "<br>"
    return render_template('index.html', message = result)


@app.route("/highest_rating", methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT MAX(rating) FROM movies")
        data = cur.fetchall()
        for row in data: 
            highest_rating = row[0]
    except Exception as exp:
        return render_template('index.html', message = exp)

    try: 
        cur.execute("SELECT title, year, actor, director, rating FROM movies where rating = " + str(highest_rating))
    except Exception as exp:
        return render_template('index.html', message = exp)

    data = cur.fetchall()
    result = ""
        
    for row in data:
        result += row[0] + " " + str(row[1]) + " " + row[2] + " " + row[3] + " " + str(row[4]) + "<br>"
    return render_template('index.html', message = result)


@app.route("/lowest_rating", methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT MIN(rating) FROM movies")
        data = cur.fetchall()
        for row in data: 
            lowest_rating = row[0]
    except Exception as exp:
        return render_template('index.html', message = exp)

    try: 
        cur.execute("SELECT title, year, actor, director, rating FROM movies where rating = " + str(lowest_rating))
    except Exception as exp:
        return render_template('index.html', message = exp)

    data = cur.fetchall()
    result = ""
        
    for row in data:
        result += row[0] + " " + str(row[1]) + " " + row[2] + " " + row[3] + " " + str(row[4]) + "<br>"
    return render_template('index.html', message = result)





@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
